import React from 'react';
import {BrowserRouter as Router,Route,Redirect} from 'react-router-dom';
import Navbar from './components/Navbar/Navbar'
import Login from './components/Login/Login';
import HomeAdmin from './components/pages/HomeAdmin'
import TablaEmpleados from './components/Tablas/TablaEmpAdmin';
import TablaContAdmin from './components/Tablas/TablaContAdmin';
import TablaAdministativos from './components/Tablas/TablaAdministativos';
import RegistroRH from './components/Registros/RegistroRH';
import RegistroEmp from './components/Registros/RegistroEmp'
import RegistroCont from './components/Registros/RegistroCont'
import TablaEmpleadosCont from './components/Tablas/TablaEmpleadosCont'
import About from './components/AcercaDe/About';


function App() {
  return (
    <Router>
      
      <div className="container pt-5">
        <Route path='/' exact component={Login}/>      
      </div>
      <Route path='/home/admin' exact component={HomeAdmin}/>
      <Route path='/home/veremp' exacat component={TablaEmpleados}/>
      <Route path='/home/vercont' exact component={TablaContAdmin}/>
      <Route path='/home/veradmin' exact component={TablaAdministativos}/>

      <Route path='/dasboard/veremp-unico/:id' exact component={TablaEmpleadosCont}/>
      
      <Route path='/registrar/rh' exact component={RegistroRH}/>
      <Route path='/editar/rh/:id' exact component={RegistroRH}/>

      <Route path='/registrar/empleados' exact component={RegistroEmp}/>
      <Route path='/editar/empleados/:id' exact component={RegistroEmp}/>

      <Route path='/registrar/contratistas' exact component={RegistroCont}/>
      <Route path='/editar/contratistas/:id'exact component={RegistroCont}/>

      <Route path='/acerca' exact component={About}/>
      

    </Router>
  );
}

export default App;

import React from 'react'
import IMG1 from '../AcercaDe/IMG/Esteban.jpeg';
import IMG2 from '../AcercaDe/IMG/Vasco.jpeg';
import IMG3 from '../AcercaDe/IMG/Leo.jpg';
import Sidebar from '../Sidebar/Sidebar';
import SecondNav from '../SecondNavbar/SecondNavbar';
import Navbar from '../Navbar/Navbar';
import SecondNavbar from '../SecondNavbar/SecondNavbar';
import Footer from '../Footer/Footer';
import './About.css'


export default function About (){
    
    return(
        <div>
            
                <div className="d-flex font">
                    <Sidebar/>
                    <div className="w-100">
                        <Navbar/>
                        <SecondNavbar/>
                        <div id="content" >
                            
                            <section clasName="bg-grey">
                               <div className="container">
                                   <div className="row">
                                       <div className="col-lg-12 my-3">
                                            <div className="card rounded-0">
                                                <div className="card-header bg-primary">
                                                    <h4 className="text-info font-weight-bold text-center text-light mb-0">¿Quiénes somos?</h4>
                                                </div>
                                                <div className="card-body">
                                                    <div className="content-box bg-white rounded-top">
                                                        
                                                        <p className="topmargin-sm cl-dark">
                                                        ELV Developers es un equipo dedicado a ofrecer servicios personalizados de desarrollo avanzado, enfocados en la programación tanto Front-End como Back-End al momento de satisfacer las necesidades de tu empresa o negocio.
                                                        
                                                        Con las mejores metodologías y los más altos estándares que garantizan la calidad tanto en el diseño como en la construcción, evaluación y pruebas de nuestras soluciones de desarrollo.
                                                        </p>
                                                        <div className="row mb-4">
                                                            <div className="col-md-10 offset-md-1">
                                                                <h1 className="display-4 text-center text-danger text-justify">Nuestro equipo
                                                                </h1>
                                                                <hr/>
                                                                <div className="card-group">
                                                                    <div className="card">
                                                                        <img src={IMG1} className="card-img-top img-fluid rounded-top" alt="Esteban Herrera" />
                                                                        <div className="card-body">
                                                                            <h5 className="card-title text-info">Esteban Herrera</h5>
                                                                            <p className="card-text">Full Stack Developer</p>
                                                                            <p className="card-text"><small className="text-muted">Contacto: <a href="mailto:esteban3008@live.com.ar"><i className="fas fa-envelope"></i></a></small></p>
                                                                        </div>
                                                                    </div>
                                                                    <div className="card">
                                                                        <img src={IMG3} className="card-img-top img-fluid rounded-top" alt="Leoana Suarez"></img>
                                                                        <div class="card-body">
                                                                            <h5 class="card-title text-info">Leoana Suárez</h5>
                                                                            <p class="card-text">Front-End Developer</p>
                                                                            <p class="card-text"><small class="text-muted">Contacto: <a href="mailto:leoana1705@gmail.com"><i className="fas fa-envelope"></i></a></small></p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="card">
                                                                        <img src={IMG2} class="card-img-top" alt="Juan Vasco"></img>
                                                                        <div className="card-body">
                                                                            <h5 className="card-title text-info rounded-top">Juan Vasco </h5>
                                                                            <p className="card-text">Back-End Developer </p>
                                                                            <p class="card-text"><small className="text-muted">Contacto: <a href="mailto:jvascosanchez@hotmail.com"><i className="fas fa-envelope"></i></a> </small></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            </div>
                                       </div>
                                   </div>
                               </div>
                            </section>
                            <Footer/>
                        </div>
                    </div>
                    
                </div>
            
         
        </div>

    )
}

import React, { useState } from 'react'
import './Login.css'
import Axios from 'axios'
import Swal from 'sweetalert2'
import {Link} from 'react-router-dom'
import Logo from './img/logo.png'

export default function Login() {

    const[email,setEmail]=useState('')
    const[password,setPassword]=useState('')
  


    const login=async(e)=>{
        e.preventDefault()

        //Obtenemos los datos ingresados
        const usuario={email,password}
      
        //DATOS DE ADMINISTRADOR 
        const admin=await Axios.post('http://localhost:4000/admin/login',usuario)
        const menAdm=admin.data.mensaje
        //console.log(admin)
        var rolAdm=undefined
        if(menAdm==='Bienvenido'){
            const respuesta=admin.data.rol
            rolAdm=respuesta
            //console.log(rolAdm)
        }

        //DATOS RECURSOS HUMANOS
        const RH=await Axios.post('http://localhost:4000/rh/login',usuario)
        const menRH=RH.data.mensaje
        var rolRH=undefined
        console.log(RH.data)
        if(menRH==='Bienvenido'){
            const respuesta=RH.data.rol
            rolRH=respuesta
            
        }
        //console.log(respuesta)

        //DATOS CONTRATISTAS
        const cont=await Axios.post('http://localhost:4000/contratistas/login',usuario)
        const menCont=cont.data.mensaje
        var rolCont=undefined
        if(menCont==='Bienvenido'){
            const respuesta=cont.data.rol
            rolCont=respuesta
        }
        
        //Datos empleados
        const emp=await Axios.post('http://localhost:4000/empleados/login',usuario)
        const menEmp=emp.data.mensaje
        //console.log(emp)
        var rolEmp=undefined
        if(menEmp==='Bienvenido'){
            const respuesta=emp.data.rol
            rolEmp=respuesta
        }


        if(rolRH==='RH'){
            //Si esto es cierto capturamos el Token y el ID
            const token=RH.data.token
            const id=RH.data.id
            const name=RH.data.name
            const rol=RH.data.rol
            const email=RH.data.email
            const imageUrl=RH.data.imageUrl

            //console.log(imageUrl)
            sessionStorage.setItem('token',token)
            sessionStorage.setItem('id',id)
            sessionStorage.setItem('name',name)
            sessionStorage.setItem('rol',rol)
            sessionStorage.setItem('email',email)
            sessionStorage.setItem('imageUrl',imageUrl)
            /* console.log(RH.data) */
            Swal.fire({
                icon: 'success',
                title: menRH,
                showConfirmButton: false
            })
            setTimeout(()=>{
                window.location.href='/home/admin'
            },1500)

        }else if(rolCont==='Contratista'){
            //Si esto es cierto capturamos el Token y el ID
            const token=cont.data.token
            const id=cont.data.id
            const name=cont.data.name
            const rol=cont.data.rol
            const email=cont.data.email
            const imageUrl=cont.data.imageUrl
            sessionStorage.setItem('token',token)
            sessionStorage.setItem('id',id)
            sessionStorage.setItem('name',name)
            sessionStorage.setItem('rol',rol)
            sessionStorage.setItem('email',email)
            sessionStorage.setItem('imageUrl',imageUrl)
            Swal.fire({
                icon: 'success',
                title: menCont,
                showConfirmButton: false
            })
            setTimeout(()=>{
                window.location.href='/admin'
            },1500)


        } else if (rolAdm==='Administrador'){
            //Si esto es cierto capturamos el Token y el ID
            const token=admin.data.token
            const id=admin.data.id
            const name=admin.data.name
            const rol=admin.data.rol
            const email=admin.data.email
            const imageUrl=admin.data.imageUrl

            sessionStorage.setItem('token',token)
            sessionStorage.setItem('id',id)
            sessionStorage.setItem('name',name)
            sessionStorage.setItem('rol',rol)
            sessionStorage.setItem('email',email)
            sessionStorage.setItem('imageUrl',imageUrl)
            Swal.fire({
                icon: 'success',
                title: menAdm,
                showConfirmButton: false
            })
            setTimeout(()=>{
                window.location.href='/home/admin'
            },1500)
        } else if(rolEmp==='Empleado'){
            const token=emp.data.token
            const id=emp.data.id
            const name=emp.data.name
            const rol=emp.data.rol
            const email=emp.data.email
            const imageUrl=emp.data.imageUrl

            sessionStorage.setItem('token',token)
            sessionStorage.setItem('id',id)
            sessionStorage.setItem('name',name)
            sessionStorage.setItem('rol',rol)
            sessionStorage.setItem('email',email)
            sessionStorage.setItem('imageUrl',imageUrl)
            Swal.fire({
                icon: 'success',
                title: menEmp,
                showConfirmButton: false
            })
            setTimeout(()=>{
                window.location.href='/home/admin'
            },1500)
        }
        else{
            Swal.fire({
                icon: 'error',
                title: 'Contraseña o correo incorrecto!',
                showConfirmButton: false,
                timer:1500
            })
            
        }
    }





    return (
        <div className="container txt-center">
            <form className="form-signin" onSubmit={login}>
                <img src={Logo} alt="Login" className="img-logo mb-4" width="72" height="72"/>
                <h1 className="h3 mb-3 font-weight-normal text-center">Inicie sesión</h1>
                <label htmlFor="inputEmail" className="sr-only">Email address</label>
                <input type="email" id="inputEmail" className="form-control" placeholder="Email address" required autoFocus onChange={e=>setEmail(e.target.value)} value={email}/>
                <label htmlFor="inputPassword" className="sr-only">Password</label>
                <input type="password" id="inputPassword" className="form-control" placeholder="Password" required onChange={e=>setPassword(e.target.value)} value={password}/>
                <div className="checkbox mb-3">
                    <label htmlFor="">
                        <input type="checkbox" value="remember-me"/> Remember me
                    </label>
                <button className="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
                <p className="mt-5 mb-3 text-muted text-center">&copy;2020 - 2021</p>
                </div>
            </form>
            
            
        </div>
    )
}

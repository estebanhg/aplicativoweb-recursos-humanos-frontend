import React, { useState, useEffect } from 'react'
import {BrowserRouter as Router,Route,Redirect,Link} from 'react-router-dom';
import Axios from 'axios'
import Swal from 'sweetalert2'
import {format} from 'timeago.js'
import Sidebar from '../Sidebar/Sidebar'
import SecondNav from '../SecondNavbar/SecondNavbar'
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/Footer'

export default function RegistroRH(props) {
    
    
    const [name,setName] = useState('')
    const [lastname,setLastname]= useState('')
    const [cc,setCc]=useState('')
    const [email,setEmail]=useState('')
    const [password,setPassword]=useState('')
    const [image,setImage]=useState('')
    const [dato,setDato]=useState('')

    const [editar,setEditar]=useState(false)


    useEffect(()=>{
        //Capturar ID
        if(props.match.params.id){
            setEditar(true)
            
        }
    },[props.match.params.id])
    
    
    const registrarUsuario=async()=>{
        
        const RHNuevo={
            name:name,
            lastname:lastname,
            cc:cc,
            email:email, //Entidad y valor del mismo nombre no es necesario
            password:password,
           
        }
        //Peticion
        const token=sessionStorage.getItem('token')
        const respuesta=await Axios.post('http://localhost:4000/rh/registrar',RHNuevo,{
          headers:{'autorizacion': 'barer '+token}
        })
        const mensaje=respuesta.data.mensaje
        Swal.fire({
            
            icon: 'success',
            title: mensaje,
            showConfirmButton: false,
            
          })
          setTimeout(()=>{
            window.location.href='/home/admin'
          },1600)
        //Luego de guardar el paciente me devuelve a la raiz 
        
    }
    
    //Actualizar RH
    const actualizar=async()=>{
        //sacar Id
        const id=props.match.params.id
        const usuario={
            name:name,
            lastname:lastname,
            cc:cc,
            email:email, //Entidad y valor del mismo nombre no es necesario
            imageUrl:dato //Dato contiene la ruta de la imagen
        }
        const token=sessionStorage.getItem('token')
        const respuesta = await Axios.put('http://localhost:4000/rh/actualizar/'+id,usuario,{
            headers:{'autorizacion': 'barer '+token}
          })
          const mensaje=respuesta.data.mensaje
          Swal.fire({
              icon: 'success',
              title: mensaje,
              showConfirmButton: false,
              
            })
            setTimeout(()=>{
              window.location.href='/home/veradmin'
            },1600)
    }
    
    const accion=async(e)=>{
        e.preventDefault()
        if(editar){
            actualizar()
        }else{
            registrarUsuario()
        }
    }
    
    //Guardar Imagen
    const guardar=async(e)=>{
        e.preventDefault()

        //Enviar datos
        const formdata= new FormData()
        formdata.append('image',image)
        const respuesta=await Axios.post('http://localhost:4000/img',formdata)
        setDato(respuesta.data.imageUrl)
    }


    return (
        <Router>
          <div className="d-flex">
                <Sidebar/>
              <div className="w-100">
                  <Navbar/>
                  <SecondNav/>
                <div id="content">
                    <section className="bg-grey">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-12 my-3">
                                <div className="card my-3">
                                    <div className="text-center">
                                            <div className="col-md-8 pt-4 mx-auto">
                                                <div className="card-header w-100 bg-primary text-light">
                                                {
                                                    editar?<h3>Actualizar RH</h3>:<h3>Registrar RH</h3>
                                                }
                                                </div>
                                                <div className="card-body">
                                                
                                                        {
                                                            editar?
                                                                <div className="card" style={{width:'15rem'}}>
                                                                    <img src={dato} alt="Profile" className="card-img"/>
                                                                    <div className="card-body">
                                                                        
                                                                        <h5 className="card-title">Foto de perfil</h5>
                                                                        <hr/>
                                                                        <form onSubmit={guardar}>
                                                                            <div className="form-group">
                                                                                <input clasName="form-control mb-2" type="file" 
                                                                                autoFocus onChange={e=>setImage(e.target.files[0])} required/>
                                                                            </div>
                                                                            <hr/>
                                                                            <div className="form-group">
                                                                                <button className="btn btn-primary" type="submit">Cargar</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>: null
                                                        }
                                            
                                                    <form onSubmit={accion}>
                                                        
                                                        
                                                        <div className="form-group pt-2">
                                                            <input type="text" className="form-control" value={name} onChange={e=>setName(e.target.value)} placeholder="Nombre " required/>
                                                        </div>
                                                        <div className="form-group">
                                                            <input type="text" className="form-control" value={lastname} onChange={e=>setLastname(e.target.value)}placeholder="Apellido " required/>
                                                        </div>
                                                        <div className="form-group">
                                                            <input type="text" className="form-control" value={cc} onChange={e=>setCc(e.target.value)}placeholder="Documento Identidad "required/>
                                                        </div>
                                                        <div className="form-group">
                                                            <input type="email" className="form-control" value={email} onChange={e=>setEmail(e.target.value)}placeholder="email@ejemplo.com" required/>
                                                        </div>
                                                        {
                                                            editar? null:
                                                            <div className="form-group">
                                                                <input type="password" className="form-control" value={password} onChange={e=>setPassword(e.target.value)} placeholder="Contraseña"required/>
                                                            </div>
                            
                                                        }
                                                        
                                                        {
                                                            editar?(
                                                            <button type="submit" className="btn btn-primary">Actualizar</button>
                                                            ): <button type="submit" className="btn btn-primary"> Guardar</button>
                                                        }
                                                    
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Footer/>
                    </section>
                </div>
              </div>
          </div>
          
        </Router>
    )
}

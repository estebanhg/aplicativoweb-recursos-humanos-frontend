import React, { useState, useEffect } from 'react'
import {BrowserRouter as Router,Route,Redirect,Link} from 'react-router-dom';
import Axios from 'axios'
import Swal from 'sweetalert2'
import {format} from 'timeago.js'
import Sidebar from '../Sidebar/Sidebar'
import SecondNav from '../SecondNavbar/SecondNavbar'
import Footer from '../Footer/Footer'
import Navbar from '../Navbar/Navbar';

export default function RegistroEmp(props) {
    
    
    const [name,setName] = useState('')
    const [lastname,setLastname]= useState('')
    const [cc,setCc]=useState('')
    const [telefono,setTelefono]=useState('')
    const [contrato,setContrato]=useState('')
    const [adress,setAdress]=useState('')
    const [nacimiento,setNacimiento]=useState('')
    const [email,setEmail]=useState('')
    const [contratistas,setContratistas]=useState([])
    const [contrastistaSeleccionado,setContratistaSeleccionado]=useState('')
    const [password,setPassword]=useState('')
    const [editar,setEditar]=useState(false)

    //Contratistas
    //const [contratistas, setContratistas] = useState([])
    const [notienecontratista,setNotieneContratista]= useState('')

    //Cargar IMAGEN
    const [image,setImage]=useState('')
    const [dato,setDato]=useState('')


    useEffect(()=>{

        //Capturar ID
        if(props.match.params.id){
            setEditar(true)
            const id=props.match.params.id
            
            
        }

        obtenerContratistas()
        
    
    },[props.match.params.id])


    
    const consultarUsuarioUnico=async(id)=>{
        const respuesta =await Axios.get('http://localhost:4000/empleados/oneUser/'+id)
        setName(respuesta.data.name)
        setLastname(respuesta.data.lastname)
        setCc(respuesta.data.cc)
        setTelefono(respuesta.data.telefono)
        setContrato(respuesta.data.contrato)
        setAdress(respuesta.data.adress)
        setNacimiento(respuesta.data.nacimiento)
        setEmail(respuesta.data.email)
        setPassword(respuesta.data.password)
        setContratistas(respuesta.data.contratistas)
        
    }



    const obtenerContratistas=async()=>{
        const token=sessionStorage.getItem('token')
        const respuesta=await Axios.get('http://localhost:4000/contratistas',{
          headers:{'autorizacion': 'barer '+token}
        })
        //console.log(respuesta)
        if(respuesta.data.length>0){
            setContratistas(respuesta.data)
            setContratistaSeleccionado(respuesta.data[0]._id)
            setNotieneContratista(false)
            
        }else{
            setNotieneContratista(true)
        }
        
    }
    
    
    const registrarUsuario=async()=>{
        
        const usuarioNuevo={
            name:name,
            lastname:lastname,
            cc:cc,
            telefono:telefono,
            contrato:contrato,
            adress:adress,
            nacimiento:nacimiento,
            email:email, //Entidad y valor del mismo nombre no es necesario
            password:password,
            contratistas:contrastistaSeleccionado,
            
        }
        
        //Peticion
        const token=sessionStorage.getItem('token')
        const respuesta=await Axios.post('http://localhost:4000/empleados/registrar',usuarioNuevo,{
          headers:{'autorizacion': 'barer '+token}
        })
        const mensaje=respuesta.data.mensaje

        if(mensaje==='El correo ya existe'){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: mensaje+'...'
                
            }
               
            )
        }else{
            Swal.fire({
            
                icon: 'success',
                title: mensaje,
                showConfirmButton: false,
                
            })
              setTimeout(()=>{
                window.location.href='/home/veremp'
              },1600)
        }
        
        //Luego de guardar el paciente me devuelve a la raiz 
        
    }
    
    //Actualizar RH
    const actualizar=async()=>{
        //sacar Id
        const id=props.match.params.id
        const usuario={
            name:name,
            lastname:lastname,
            cc:cc,
            telefono:telefono,
            contrato:contrato,
            adress:adress,
            nacimiento:nacimiento,
            email:email, //Entidad y valor del mismo nombre no es necesario
            contratistas:contrastistaSeleccionado,
            imageUrl:dato

        }
        const token=sessionStorage.getItem('token')
        const respuesta = await Axios.put('http://localhost:4000/empleados/actualizar/'+id,usuario,{
            headers:{'autorizacion': 'barer '+token}
          })
          const mensaje=respuesta.data.mensaje
          Swal.fire({
              
              icon: 'success',
              title: mensaje,
              showConfirmButton: false,
              
            })
            setTimeout(()=>{
              window.location.href='/home/veremp'
            },1600)
    }
    
    const accion=async(e)=>{
        e.preventDefault()
        if(editar){
            actualizar()
        }else{
            registrarUsuario()
        }
    }
    
    //Guardar Imagen
    const guardar=async(e)=>{
        e.preventDefault()

        //Enviar datos
        const formdata= new FormData()
        formdata.append('image',image)
        const respuesta=await Axios.post('http://localhost:4000/img',formdata)
        setDato(respuesta.data.imageUrl)
    }
    

    return (
        <Router>
          <div className="d-flex">
            <Sidebar/>
              <div className="w-100">
                  <Navbar/>
                  <SecondNav/>
                  <div id="content">
                    <section className="bg-grey">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-12 my-3">
                                <div className="card my-3">
                                <div className="containter text-center">
                                    <div className="col-md-8 pt-4 mx-auto">
                                        <div className="car card-body">
                                            <div className="card-header bg-primary text-light">
                                            {
                                                editar?<h3>Actualizar Empleados</h3>:<h3>Registrar Empleados</h3>
                                            }
                                             </div>
                                    
                                            {
                                                editar?
                                                    <div className="card" style={{width:'15rem'}}>
                                                        <img src={dato} alt="Profile" className="card-img"/>
                                                        <div className="card-body">
                                                            
                                                            <h5 className="card-title">Foto de perfil</h5>
                                                            <hr/>
                                                            <form onSubmit={guardar}>
                                                                <div className="form-group">
                                                                    <input clasName="form-control mb-2" type="file" 
                                                                    autoFocus onChange={e=>setImage(e.target.files[0])} required/>
                                                                </div>
                                                                <hr/>
                                                                <div className="form-group">
                                                                    <button className="btn btn-primary" type="submit">Cargar</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>: null
                                            }
                        
                                            <form onSubmit={accion}>
                                                <div className="form-group">
                                                    <input type="text" className="form-control" value={name} onChange={e=>setName(e.target.value)} placeholder="Nombre " required/>
                                                </div>
                                                <div className="form-group">
                                                    <input type="text" className="form-control" value={lastname} onChange={e=>setLastname(e.target.value)}placeholder="Apellido " required/>
                                                </div>
                                                <div className="form-group">
                                                    <input type="text" className="form-control" value={cc} onChange={e=>setCc(e.target.value)}placeholder="CC "required/>
                                                </div>
                                                <div className="form-group">
                                                    <input type="text" className="form-control" value={telefono} onChange={e=>setTelefono(e.target.value)}placeholder="Telefono "required/>
                                                </div>
                                                <div className="form-group">
                                                    <input type="text" className="form-control" value={contrato} onChange={e=>setContrato(e.target.value)}placeholder="Tipo contrato "required/>
                                                </div>
                                                <div className="form-group">
                                                    <input type="text" className="form-control" value={adress} onChange={e=>setAdress(e.target.value)}placeholder="Direccion "required/>
                                                </div>
                                                <div className="form-group">
                                                <div className="form-group">
                                                    <input type="text" className="form-control" value={nacimiento} onChange={e=>setNacimiento(e.target.value)}placeholder="Fecha nacimiento "required/>
                                                </div>
                                                    <input type="email" className="form-control" value={email} onChange={e=>setEmail(e.target.value)}placeholder="email@ejemplo.com" required/>
                                                </div>
                                                {
                                                        editar? null:
                                                        <div className="form-group">
                                                            <input type="password" className="form-control" value={password} onChange={e=>setPassword(e.target.value)} placeholder="Contraseña"required/>
                                                        </div>
                
                                                }
                                                <div className="form-group">
                                                    <div className="input-group mb-3">
                                                        <div className="input-group-prepend">
                                                            <label className="input-group-text" htmlFor="inputGroupSlect01">Contratista:</label>
                                                        </div>
                                                        <select className="custom-select" id="inputGroupSelect01" onChange={e=>setContratistaSeleccionado(e.target.value)} >
                                                            {
                                                                notienecontratista? (
                                                                <option selected>No hay contratistas</option>):
                                                                contratistas.map((user,i)=>(
                                                                    <option key={i} value={user._id}>
                                                                        {user.name}
                                                                    </option>
                                                                
                                                                ))
                                                            }
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                {
                                                    editar?(
                                                    <button type="submit" className="btn btn-primary">Actualizar</button>
                                                    ): <button type="submit" className="btn btn-primary"> Guardar</button>
                                                }
                                                        
                                            </form>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <Footer/>
                    </section>
                    
                  </div>
              </div>
          </div>
          
        </Router>
    )
}

import React, { useState, useEffect } from 'react'
import {Link} from 'react-router-dom'
import './Sidebar.css'
import Avatar from '../Sidebar/img/avatar.jpeg'

export default function Sidebar() {
    
    const [imagen,setImagen]=useState(false)

    //Permisos para las opciones
    const [permiso,setPermiso]=useState(false)
    

    useEffect(()=>{
        
        validaringreso()
        validar()
    },[])
    


    const validar=()=>{
        const dato = sessionStorage.getItem('imageUrl')
        if(dato!=="undefined"){
            setImagen(true)
            console.log(dato)
        }else{
            setImagen(false)
        }
    }

    const validaringreso=async()=>{

        const rol=sessionStorage.getItem('rol')
        const name=sessionStorage.getItem('name')
        const email=sessionStorage.getItem('email')
  
        if (email==='admin@admin.com'){
            //Datos 2 permisos Eliminar -- Ver y actualizar
            setPermiso(true)
            
        }else if(name!=='admin' && rol==='RH'){
  
           //Quiere decir que es un usuario de recursos humanos 1 permiso -> Ver y Actualizar 
           setPermiso(true)
        }else {
          //Si no es ninguno de los 2 negamos el permiso 
          setPermiso(false)
          
        }
    }

    return (
       

            <div id="sidebar-container" className="bg-primary">
                <div className="logo">
                    <h4 className="text-light font-weight-bold">DASBOARD</h4>
                </div>
                <div className="menu">

                    <div className="card posicion" style={{width:'10rem'}}>
                        {
                            imagen?
                                <img src={sessionStorage.getItem('imageUrl')} 
                                alt="Foto perfil" className="card-img"/>:
                                <img src={Avatar} alt="Foto perfil" className="card-img"/>
                        }
                        
                        <div className="card-body">
                                
                            <h6 className="card-title">{sessionStorage.getItem('rol')}</h6>
                        </div>
                    </div>
                    
                
                    {
                        permiso ?
                            <div>
                                <span className="d-block text-light p-2 font-weight-bold">Navegacion</span>
                                <a href="/home/admin" className="d-block text-light p-4"><i className="fas fa-home"></i> Inicio</a>
                                <a href="/home/veradmin" className="d-block text-light p-4"><i className="fas fa-user-lock"></i> Administrativo</a>
                                <a href="/home/vercont" className="d-block text-light p-4"><i className="fas fa-briefcase"></i> Contratistas</a>
                                <a href="/home/veremp" className="d-block text-light p-4"><i className="fas fa-hard-hat"></i> Empleados</a>
                                {/* <a href="#" className="d-block text-light p-4"><i className="fas fa-cog"></i> Configuracion</a> */}
                            </div>: 
                            <div>
                                <span className="d-block text-light p-2 font-weight-bold">Navegacion</span>
                                <a href="/home/admin" className="d-block text-light p-4"><i className="fas fa-home"></i> Inicio</a>
                                <a href="#" className="d-block text-light p-4">Crear Tareas</a>
                                {/* <a href="#" className="d-block text-light p-4"><i className="fas fa-cog"></i> Configuracion</a> */}
                            </div>
                    }
                </div>
            
            </div>
        
    )
}

import React, {useState, useEffect} from 'react'
import {BrowserRouter as Router,Route,Redirect,Link} from 'react-router-dom';
import Axios from 'axios'
import {format} from 'timeago.js'
import Sidebar from '../Sidebar/Sidebar'
import SecondNav from '../SecondNavbar/SecondNavbar'
import Swal from 'sweetalert2'
import Footer from '../Footer/Footer'
import Navabar from '../Navbar/Navbar'
import './Tabla.css'

export default function TablaEmpleados(props) {
    const [empleados, setEmpleados] = useState([])
    const [notienempleados,setNotieneEmpleados]= useState('')
    

    //Permisos para Eliminar o editar
    const [permiso,setPermiso]=useState(false)
    const [permisoAdmin,setPermisoAdmin]=useState(false)

    useEffect(()=>{ //cuando el componente se cargue, se mostraran lo solicitado
        obtenerEmpleados()
        
        //Validar permisos
        validaringreso()

        
      }, [props.match.params.id])

    //Funcion para listar Usuarios
    const obtenerEmpleados=async()=>{

        //Capturamos ID
        const id=props.match.params.id

        const token=sessionStorage.getItem('token')
        const respuesta=await Axios.get('http://localhost:4000/empleadoscontratistas/'+id,{
          headers:{'autorizacion': 'barer '+token}
        })

        console.log(respuesta)
        if(respuesta.data.length>0){
            setEmpleados(respuesta.data)
            setNotieneEmpleados(false)
            
        }else{
            setNotieneEmpleados(true)
        }
        
    }

    const eliminarUsuario=async(id)=>{
      const token=sessionStorage.getItem('token')
      const respuesta=await Axios.delete('http://localhost:4000/empleados/eliminar/'+id,{
        headers:{'autorizacion': 'barer '+token}
      })

      Swal.fire({
            
        icon: 'success',
        title: 'Eliminado Correctamente',
        showConfirmButton: false,
        
    })
      setTimeout(()=>{
        window.location.href='/admin/veremp'
      },1600)
    }

   

    //PERMISOS DE USUARIOS 
    const validaringreso=async()=>{

      const rol=sessionStorage.getItem('rol')
      const name=sessionStorage.getItem('name')
      const email=sessionStorage.getItem('email')

      if (email==='admin@admin.com'){
          //Datos 2 permisos Eliminar -- Ver y actualizar
          setPermiso(true)
          setPermisoAdmin(true)
      }else if(name!=='admin' && rol==='RH'){

         //Quiere decir que es un usuario de recursos humanos 1 permiso -> Ver y Actualizar 
         setPermiso(true)
      }else {
        //Si no es ninguno de los 2 negamos el permiso 
        setPermiso(false)
        setPermisoAdmin(false)
      }
    }


    return (
          <Router>
          <div className="d-flex">
            <Sidebar/>
              <div className="w-100">
                <Navabar/>
                <SecondNav/>
                  <div id="content"> 
                    
                    <section className="bg-grey">
                      <div className="container">
                        <div className="row">
                          <div className="col-lg-12 my-3">
                            <div className="card">
                              <div className="card-header bg-primary">
                                <h4 className="text-info font-weight-bold text-center text-light mb-0"><i className="fas fa-users"></i> Empleados</h4>
                              </div>
                                <div className="card-body">
                                        {
                                  notienempleados ?
                                    <div className="container text-center pt-4">
                                        <div className="card border-0">
                                            <div className="card-header">
                                                <h4>Aun no tiene empleados registrados</h4>
                                            </div>
                                        </div>
                                      </div> : 
                                      <div> 
                                        
                                        <div className="table table-responsive">
                                          <thead className="table-light">
                                            <tr>
                                              <th>#</th>
                                              <th>Nombre</th>
                                              <th>Apellido</th>
                                              <th>Cedula</th>
                                              <th>Telefono</th>
                                              <th>Contrato</th>
                                              <th>Direccion</th>
                                              <th>Nacimiento</th>
                                              <th>Correo</th>
                                              <th>Contratista</th>
                                              <th>Opciones</th>
                                              <th></th>
                                            </tr>
                                          </thead>
                                          {
                                            
                                            empleados.map((user,i)=>(
                                              <tbody key={user._id}>
                                                <tr >
                                                  <td>{i + 1}</td>
                                                  <td>{user.name}</td>
                                                  <td>{user.lastname}</td>
                                                  <td>{user.cc}</td>
                                                  <td>{user.telefono}</td>
                                                  <td>{user.contrato}</td>
                                                  <td>{user.adress}</td>
                                                  <td>{format(user.nacimiento)}</td>
                                                  <td>{user.email}</td>
                                                  <td >{user.contratistas.name}</td>
                                                  {
                                                    permiso ? 
                                                      <td>
                                                        <a className="btn btn-outlie-suces" href={'/editar/empleados/'+user._id}>
                                                          <i className="fas fa-user-edit"></i>
                                                        </a>
                                                      </td> : ''
                                                  }
                                                  {
                                                    permisoAdmin ? 
                                                      <td>
                                                          <button className="btn btn-outline-danger" onClick={()=>eliminarUsuario(user._id)}>
                                                            <i className="far fa-trash-alt"></i>
                                                        </button>
                                                      </td> : ''
                                                  }
                                                </tr>
                                                
                                              </tbody>
                                            ))

                                            }
                                          </div>
                                      </div>
                                    }
                                </div>
                              
                            </div>
                          </div>
                        </div>
                        
                      </div>
                      <Footer/>
                    </section>
                    
                    
                    
                  </div>
              </div>
              <div className="row">
                  <div className="col-md-9 ml-sm-auto col-lg-10 px-md-4">
                    <Footer/>
                  </div>
                
              </div>
              
          </div>
          
        </Router>
        
    )
}

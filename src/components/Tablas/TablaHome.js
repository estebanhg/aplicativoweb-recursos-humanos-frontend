import React, {useState, useEffect} from 'react'
import Axios from 'axios'
import {format} from 'timeago.js'

export default function TablaHome() {

  const [empleados, setEmpleados] = useState([])
  const [contratistas, setContratistas] = useState([])
  const [recursosH, setrecursosH] = useState([])

    useEffect(()=>{ //cuando el componente se cargue, se mostraran lo solicitado
      obtenerEmpleados()
      obtenerContratistas()
      obtenerRecursosH() 
    }, [])// evitando un bucle infinito

    const obtenerEmpleados=async()=>{
        const token=sessionStorage.getItem('token')
        const respuesta=await Axios.get('http://localhost:4000/empleados',{
          headers:{'autorizacion': 'barer '+token}
        })
        //console.log(respuesta)
        setEmpleados(respuesta.data)
    }

   const obtenerContratistas=async()=>{
    const token=sessionStorage.getItem('token')
        const respuesta=await Axios.get('http://localhost:4000/contratistas',{
          
            headers:{'autorizacion': 'barer '+token}
        })
        //console.log(respuesta)
        setContratistas(respuesta.data)
    }

    const obtenerRecursosH=async()=>{
      const token=sessionStorage.getItem('token')
        const respuesta=await Axios.get('http://localhost:4000/rh',{
          headers:{'autorizacion': 'barer '+token}
        })
        // console.log(respuesta)
        setrecursosH(respuesta.data)

    } 
    return (
        <div className="container">
          <h4 className="text-info"><i className="fas fa-users"></i> Empleados</h4>
          <table className="table table-responsive">
            <thead className="thead-dark">
              <tr>
                <th >#</th>
                <th >Nombre</th>
                <th >Apellido</th>
                <th >Cedula</th>
                <th >Teléfono</th>
                <th>Contrato</th>
                <th>Dirección</th>
                <th>Nacimiento</th>
                <th>Correo</th>
                <th>Contratista</th>
              </tr>
              </thead>
              {
                empleados.map((user,i) => (
                <tbody key={user._id}>
                      <tr>
                        <td>{i + 1}</td>
                        <td>{user.name}</td>
                        <td>{user.lastname}</td>
                        <td>{user.cc}</td>
                        <td>{user.telefono}</td>
                        <td>{user.contrato}</td>
                        <td>{user.adress}</td>
                        <td>{format(user.nacimiento)}</td>
                        <td>{user.email}</td>
                        <td>{user.contratistas.name}</td>
                      </tr>
                  </tbody>
                ))
              }
          </table>
          <h4 className="text-info"><i className="fas fa-users"> Contratistas</i></h4>
        <table className="table table-responsive">
          <thead className="thead-light">
            <tr>
                <th >#</th>
                <th >Nombre</th>
                <th >Apellido</th>
                <th >Cedula</th>
                <th >Teléfono</th>
                <th > Empresa</th>
                <th > Email</th>
              </tr>
            </thead>
            {
              contratistas.map((usuario,i) => (
              <tbody key={usuario._id}>
                    <tr>
                      <td>{i + 1}</td>
                      <td>{usuario.name}</td>
                      <td>{usuario.lastname}</td>
                      <td>{usuario.cc}</td>
                      <td>{usuario.telefono}</td>
                      <td>{usuario.empresa}</td>
                      <td>{usuario.email}</td>
                    </tr>
                </tbody>
              ))
            }
        </table>
        <h4 className="text-info"><i className="fas fa-users"> Administrativos</i></h4>
        <table className="table table-responsive">
          <thead className="thead-light">
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>Cedula</th>
              <th>Correo</th>
            </tr>
          </thead>
          {
              recursosH.map((usuario,i) => (
              <tbody key={usuario._id}>
                    <tr>
                      <td>{i + 1}</td>
                      <td>{usuario.name}</td>
                      <td>{usuario.lastname}</td>
                      <td>{usuario.cc}</td>
                      <td>{usuario.email}</td>
                    </tr>
                </tbody>
              ))
        }
        </table>

</div>

    )
}

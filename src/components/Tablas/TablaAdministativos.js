import React, {useState, useEffect} from 'react'
import {BrowserRouter as Router,Route,Redirect,Link} from 'react-router-dom';
import Axios from 'axios'
import {format} from 'timeago.js'
import Sidebar from '../Sidebar/Sidebar'
import SecondNav from '../SecondNavbar/SecondNavbar'
import Swal from 'sweetalert2'
import Footer from '../Footer/Footer'
import Navabar from '../Navbar/Navbar'
import './Tabla.css'

export default function TablaAdministativos() {
    const [administrativos, setadministrativos] = useState([])
    const [notieneadministrativos,setNotieneAdministrativo]= useState('')
    

    //Permisos para Eliminar o editar
    const [permiso,setPermiso]=useState(false)
    const [permisoAdmin,setPermisoAdmin]=useState(false)

    useEffect(()=>{ //cuando el componente se cargue, se mostraran lo solicitado
        obteneradministrativos()

        //Validacion
        validaringreso()
        
      }, [])

    const obteneradministrativos=async()=>{
        const token=sessionStorage.getItem('token')
        const respuesta=await Axios.get('http://localhost:4000/rh',{
          headers:{'autorizacion': 'barer '+token}
        })
        if(respuesta.data.length>0){
            setadministrativos(respuesta.data)
            setNotieneAdministrativo(false)
            
            
            
            
        }else{
            setNotieneAdministrativo(true)
        }
        
        
        
    }

    const eliminar=async(id)=>{
      
      const token=sessionStorage.getItem('token')
      const respuesta=await Axios.delete('http://localhost:4000/rh/eliminar/'+id,{
        headers:{'autorizacion': 'barer '+token}
      })
      
        Swal.fire({
            
          icon: 'success',
          title: 'Eliminado Correctamente',
          showConfirmButton: false,
          timer: 1500
          
      })
        
          obteneradministrativos()
        

    }

    //Control de opciones 
    const validaringreso=async()=>{

      const rol=sessionStorage.getItem('rol')
      const name=sessionStorage.getItem('name')
      const email=sessionStorage.getItem('email')

      if (email==='admin@admin.com'){
          //Datos 2 permisos Eliminar -- Ver y actualizar
          setPermiso(true)
          setPermisoAdmin(true)
      }else if(name!=='admin' && rol==='RH'){

         //Quiere decir que es un usuario de recursos humanos 1 permiso -> Ver y Actualizar 
         setPermiso(true)
      }else {
        //Si no es ninguno de los 2 negamos el permiso 
        setPermiso(false)
        setPermisoAdmin(false)
      }
    }

    //Busqueda
    const Buscar=async(e)=>{
      if (e.target.value===''){
        return obteneradministrativos()
      }

      const buscar=e.target.value
      const token=sessionStorage.getItem('token')

      const respuesta=await Axios.get('http://localhost:4000/rh/buscar/'+buscar,{
            headers:{'autorizacion': 'bearer '+token}
        })
      
        // Validacion que el arreglo no este vacido
      if(respuesta.data.length>0){
          setadministrativos(respuesta.data)
          setNotieneAdministrativo(false)
          
      }else{
          setNotieneAdministrativo(true)
      }
    }
    

    return (
        
          <Router>
          <div className="d-flex">
                  <Sidebar/>
                  <div className="w-100">
                    <Navabar/>
                    <SecondNav/>
                    <div id="content">
                      <>
                          <nav className='navbar py-2'>
                              <div className="container">
                                <div className="col-md-6 ml-auto">
                                  <div className="input-group">
                                    <div className="input-group-prepend">
                                      <span className="input-group-text"><i class="fas fa-search"></i></span>
                                    </div>
                                    <input type="search" className="form-control mr-sm-2" placeholder="Buscar..."
                                      onChange={(e)=>Buscar(e)}/>
                                  </div>
                                </div>
                                  
                              </div>
                          </nav>
                      </>
                      <section className="bg-grey">
                        <div className="container">
                            <div className="row">
                              <div className="col-lg-12 my-3">
                                <div className="card rounded-0">
                                  <div className="card-header bg-primary">
                                    <h4 className="text-info font-weight-bold text-center text-light mb-0"><i className="fas fa-users"></i> Administrativos</h4>
                                  </div>
                                  <div className="card-body">
                                    {
                                    notieneadministrativos ?
                                      <div className="container text-center pt-4">
                                          <div className="card border-0">
                                              <div className="card-header">
                                                  <h4>Aun no tiene administrativos registrados</h4>
                                              </div>
                                          </div>
                                        </div> : 
                                        <div> 
                                          
                                          <div className="table table-responsive">
                                            <thead className="table-light">
                                              <tr>
                                                <th>#</th>
                                                <th>Nombre</th>
                                                <th>Apellido</th>
                                                <th>Cedula</th>
                                                <th>Correo</th>
                                                <th>Opciones</th>
                                              </tr>
                                            </thead>
                                            {
                                              
                                              administrativos.map((user,i)=>(
                                        
                                                <tbody key={user._id}>
                                                  <tr>
                                                    <td>{i + 1}</td>
                                                    <td>{user.name}</td>
                                                    <td>{user.lastname}</td>
                                                    <td>{user.cc}</td>
                                                    <td>{user.email}</td>

                                                    { user.email !=='admin@admin.com'   ?
                                                        <td>
                                                          {
                                                            permiso ? (
                                                              <a className="btn btn-outlie-sucess mr-2" href={'/editar/rh/'+user._id}>
                                                                <i className="fas fa-user-edit"></i>
                                                              </a>) : ''
                                                          }
                                                          {
                                                            permisoAdmin ? (
                                                              <button className="btn btn-outline-danger" onClick={()=>eliminar(user._id)}>
                                                                <i className="far fa-trash-alt"></i>
                                                              </button>): ''
                                                          }
                                                            
                                                        </td> : ''
                                                    }
                                                  </tr>
                                                  
                                                </tbody>
                                              ))

                                            }
                                          </div>
                                      </div>
                                    }
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        
                      </section>
                      <Footer/>
                    </div>
                    
                  </div>
              
          </div>
          
        </Router>
        
    )
}

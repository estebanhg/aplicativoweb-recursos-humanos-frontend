import React from 'react';
import {BrowserRouter as Router,Route,Redirect} from 'react-router-dom';
import Sidebar from '../Sidebar/Sidebar'
import SecondNav from '../SecondNavbar/SecondNavbar'
import Grafica from '../Grafica/Grafica'
import TablaHome from '../Tablas/TablaHome'
import Footer from '../Footer/Footer'
import Navabar from '../Navbar/Navbar'
import './Home.css'

//import Grafica from '../Grafica/Grafica'

function Home() {
  return (
    <Router>
      <div className="d-flex">
            <Sidebar/>
            <div className="w-100">
              < Navabar/>
              <div id="content">
                <SecondNav/>
                <Grafica/>
                
              </div>
            </div>   
      </div>
            
    </Router>
  );
}

export default Home;
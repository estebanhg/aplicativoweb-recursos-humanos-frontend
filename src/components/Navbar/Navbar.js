import React, { useState, useEffect } from 'react'
import {Link} from 'react-router-dom'
import './Navbar.css'

export default function Navbar() {
    
    const [menu,setMenu]=useState(false)

    useEffect(()=>{
        if (sessionStorage.getItem('token')){
            setMenu(true)
        }
    },[])


    //Borrar datos de sesion
    const salir=()=>{
        sessionStorage.clear()
        setMenu(false)
    }
    
    
    
    
    
    return (
       
          <div>
              {
                  menu ? 
                    <nav className="navbar navbar-expand-lg navbar-light bg-light border-bottom" >
                       
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                       
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                
                            <ul className="navbar-nav ml-auto">
                                <li class="nav-item dropdown font-weight-bold">
                                    <a class="nav-link dropdown-toggle"  id="navbarDropdown" role="button" data-toggle="dropdowm" aria-haspoup="true" aria-expanded="false">{sessionStorage.getItem('email')}</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="#">Editar perfil</a>
                                        <a class="dropdown-item" href="/" onClick={()=>salir()}>Salir</a>
                                            
                                    </div>
                                </li>
                                <li className="nav-item font-weight-bold">
                                    <a className="nav-link" href='/' onClick={()=>salir()}>Salir</a>
                                </li>
                            </ul>                     
                        </div>
                    </nav> :
                    <nav className="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                            <a className="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">LOGO</a>
                        </div>
                    </nav>

              }

          </div>
       
    )
}

import React from 'react'
import './Footer.css'

export default function Footer() {
  return (
    <footer className="bg-white pt-4">
      <div className="container-fluid text-center text-md-left">
        <div className="row">
          <div className="col-md-4 mt-md-0 mt-3" >
          <p className="font-weight-bold text-info ml-4" id="cl-grey"> &copy; 2020 - 2021  ELV Developers</p>
          </div>
          <div className="col-md-8 mb-md-0 mb-3">
            <h6 className="font-weight-normal"> <a href="/acerca" className="text-danger"><i className="fas fa-code"></i> Acerca de nosotros</a></h6> 
          </div>
        </div>
      </div>
    </footer>
  )
}






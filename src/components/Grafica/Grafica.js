import React, { useState, useEffect } from 'react'
import {Line, Bar} from 'react-chartjs-2'
import Axios from 'axios'
import './Grafica.css'
import Footer from '../Footer/Footer'


export default function Grafica() {

  const [charData,setChartdata]=useState({})

  
  useEffect(()=>{
    
    //Renderizar Grafica
    
    char()


  },[])

 



  const char=async()=>{


    //empleados
    const token=sessionStorage.getItem('token')
    const respuesta=await Axios.get('http://localhost:4000/empleados',{
      headers:{'autorizacion': 'barer '+token}
    })
    const empleados=respuesta.data.length
    
    //Contratistas
    const respuesta2=await Axios.get('http://localhost:4000/contratistas',{
      headers:{'autorizacion': 'barer '+token}
    })
    const contratistas=respuesta2.data.length

    //Administrativo
    const respuesta3=await Axios.get('http://localhost:4000/rh',{
          headers:{'autorizacion': 'barer '+token}
        })
    const administrativo=respuesta3.data.length
    
    
    var cont=contratistas
    var emp=empleados
    var admin=administrativo


    setChartdata({
      
      labels: ['Administrativos', 'Contratistas', 'Empleados'],
      datasets: [
        {
          label: 'Usuarios',
          backgroundColor: '#172b4d',
          borderColor: '#f4f5f7',
          data: [admin,contratistas, emp],
          borderwidth:4
      }
      
    ]
    })
  }

  
  



    return (
       <section className="bg-grey">
         <div className="container">
           <div className="row">
             <div className="col-lg-12 my-3">
               <div className="card rounded-0">
                 <div className="card-header bg-primary">
                   <h6 className="font-weight-bold mb-0 text-center text-light">Grafica de usuarios</h6>
                 </div>
                 <div className="card-body">
                    <Bar  data={charData} options={{
                      responsive:true
                    }}/>
                 </div>
               </div>
             </div>
             <div className="col-lg-4 my-3">

             </div>
           </div>
         </div>
         <Footer/>
       </section>
       
    )
}

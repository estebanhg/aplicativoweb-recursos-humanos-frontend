import React, { useState, useEffect } from 'react'
import {Link} from 'react-router-dom'
import Axios from 'axios'
import './SecondNavbar.css'
import Grafica from '../Grafica/Grafica'



export default function SecondNavbar() {

    const [permiso,setPermiso]=useState(false)

    //Administrativos
    const [administrativos, setadministrativos] = useState([])
    const [notieneadministrativos,setNotieneAdministrativo]= useState(false)

    //Contratistas
    const [contratistas, setContratistas] = useState([])
    const [notienecontratista,setNotieneContratista]= useState('')

    //Empleados
    const [empleados, setEmpleados] = useState([])
    const [notienempleados,setNotieneEmpleados]= useState('')

    useEffect(()=>{
        validaringreso()
        
        //Obtener Usuarios
        obteneradministrativos()
        obtenerContratistas()
        obtenerEmpleados()
    },[])
    
    
    const validaringreso=async()=>{

        const rol=sessionStorage.getItem('rol')
        const name=sessionStorage.getItem('name')

        if (name==='admin' || rol==='RH'){
            setPermiso(true)
        }else{
            setPermiso(false)
        }
        

    }

    //Obtener administrativos
    const obteneradministrativos=async()=>{
        const token=sessionStorage.getItem('token')
        const respuesta=await Axios.get('http://localhost:4000/rh',{
          headers:{'autorizacion': 'barer '+token}
        })
        console.log(respuesta.data.length)
        if(respuesta.data.length>0){
            
            setadministrativos(respuesta.data.length)
            setNotieneAdministrativo(false)
            
            
            
            
        }else{
            setNotieneAdministrativo(true)
        }
        
        
        
    }

    //CONTRATISTAS
    const obtenerContratistas=async()=>{
        const token=sessionStorage.getItem('token')
        const respuesta=await Axios.get('http://localhost:4000/contratistas',{
          headers:{'autorizacion': 'barer '+token}
        })
        if(respuesta.data.length>0){
            setContratistas(respuesta.data.length)
            setNotieneContratista(false)
            
        }else{
            setNotieneContratista(true)
        }
        
    }

    //Empleados
    const obtenerEmpleados=async()=>{
        const token=sessionStorage.getItem('token')
        const respuesta=await Axios.get('http://localhost:4000/empleados',{
          headers:{'autorizacion': 'barer '+token}
        })
        if(respuesta.data.length>0){
            setEmpleados(respuesta.data.length)
            setNotieneEmpleados(false)
            
        }else{
            setNotieneEmpleados(true)
        }
        
    }



    return (
        <div >
            <section className="py-3">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-9">
                            <h1 className="font-weight-bold mb-0">Bienvenido {sessionStorage.getItem('name')}</h1>
                            <p className="lead text-muted">Aquí puedes econtrar toda la información. </p>
                        </div>
                        <div className="col-lg-3 d-flex">
                        {
                            permiso ? 
                            <div className="btn-group mr-2 align-self-center">
                                <a type="submit" className="btn btn-sm btn-outline-primary text-light " href="/registrar/rh"><i class="fas fa-plus-circle"></i> RH</a>
                                <a type="button" className="btn btn-sm btn-outline-primary text-light" href="/registrar/contratistas"><i class="fas fa-plus-circle"></i> Contratista</a>
                                <a type="button" className="btn btn-sm btn-outline-primary text-light" href="/registrar/empleados"><i class="fas fa-plus-circle"></i> Empleado</a>
                            </div> : ''
                        }
                        </div>
                    </div>
                </div>
            </section>
            {
                permiso?
                    <section className="bg-mix">
                        <div className="container">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-lg-3 d-flex stat my-3">
                                            <div className="mx-auto">
                                                <h6 className="text-muted">Administrativos</h6>
                                                <h6 className="font-weight-bold"> {administrativos} Usuarios</h6>
                                                <h6 className="text-success"><i class="fas fa-plus-circle"></i> {((administrativos)/(administrativos+contratistas+empleados))*100} %</h6>
                                            </div>
                                        </div>
                                        <div className="col-lg-3 d-flex stat my-3">
                                            <div className="mx-auto">
                                                <h6 className="text-muted">Contratistas</h6>
                                                <h6 className="font-weight-bold"> {contratistas} Usuarios</h6>
                                                <h6 className="text-success"><i class="fas fa-plus-circle"></i> {((contratistas)/(administrativos+contratistas+empleados))*100} %</h6>
                                            </div>
                                        </div>
                                        <div className="col-lg-3 d-flex stat my-3">
                                            <div className="mx-auto">
                                                <h6 className="text-muted">Empleados</h6>
                                                <h6 className="font-weight-bold"> {empleados} Usuarios</h6>
                                                <h6 className="text-success"> <i class="fas fa-plus-circle"></i> {((empleados)/(administrativos+contratistas+empleados))*100} %</h6>
                                            </div>
                                        </div>
                                        <div className="col-lg-3 stat my-3">
                                            <div className="mx-auto">
                                                <h6 className="text-muted">Total Usuarios</h6>
                                                <h6 className="font-weight-bold"> {empleados+administrativos+contratistas} Usuarios</h6>
                                                <h6 className="text-success"> <i class="fas fa-plus-circle"></i> {((administrativos+contratistas+empleados)/(administrativos+contratistas+empleados))*100} %</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section> : ''
            }
        </div>
    )
}
